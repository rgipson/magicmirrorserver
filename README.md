# MagicMirrorServer #



### What does this do? ###

Provides a server and interface for configuring MagicMirrorClient(s) on the network.

### How do I get set up? ###

Requires NodeJS (trying to stick with current LTS versions).

I'll be setting up a way to "install" this if I ever take this public. Until then, simply do:

```
#!bash

node app.js start
```