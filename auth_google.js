// Portions of this code were taken from here: https://github.com/google/google-api-nodejs-client/blob/master/samples/oauth2.js
// With the license below:
//
// Copyright 2012-2016, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Private objects
var google = require('../lib/googleapis.js');
var OAuth2Client = google.auth.OAuth2;
var oAuthClient;

function getOAuthClient(config) {
    if(typeof oAuthClient === 'undefined')
    {
        oAuthClient = new OAuth2Client(config.client_id, config.client_secret, config.redirect_url);
    }

    return oAuthClient;
}

// Public objects
module.exports = {
    // Retrieves a consent page for a user.
    getAuthURL: function(config) {
        // generate consent page url
        return getOAuthClient(config).generateAuthUrl({
            access_type: 'offline', // will return a refresh token
            scope: 'https://www.googleapis.com/auth/plus.me' // can be a space-delimited string or an array of scopes
        });
    },
    loadAccessToken: function(code, config, callback) {
        var client = getOAuthClient(config);

        // request access token
        client.getToken(code, function (err, tokens) {
            if (err) {
                return callback(err);
            }
            client.setCredentials(tokens);
            callback(tokens);
        });
    },
    loadPlusProfile: function(userId, callback) {
        var plus = google.plus('v1');

        plus.people.get({ userId: 'me', auth: getOAuthClient() }, function (err, profile) {
            if (err) {
                console.log('An error occurred while retrieving the access token for Google Plus. ', err);
                callback(false);
                return;
            }

            callback(profile);
        });
    },
};