console.log('Using Node ' + process.version);

var fs = require('fs'); // File system functions
var app = require('express')();
var server = require('http').createServer(app); // express HTTP server
var path = require('path');
var io = require('socket.io').listen(server);
var pathToSettings = path.join(__dirname + '/settings.json');
var settings;

// Initializes saved settings
fs.stat(pathToSettings, function(err, stats) {
	// Create new settings.json from template
	if(err || !stats.isFile()) {
		var defaultSettings = JSON.parse(fs.readFileSync(path.join(__dirname + '/settings_default.json'), 'utf8'));
		settings = defaultSettings;
		console.log("Settings file does not exists. Is this the first time the server is being ran?");
		console.log("Setting up default settings.");
		saveSettings();
	} else {
		settings = JSON.parse(fs.readFileSync(pathToSettings), 'utf8'); // Settings file for the server, read async
	}

	// Start server once settings are loaded.
	startWebServer();
});

function getFormattedDate() {
    var date = new Date();
	
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return month + "-" + day + "-" + year + " " + hour + ":" + min + ":" + sec;
}

/**
 * Saves the settings object in its current state to the settings.json file.
 * This occurs synchronized (no async)
 * @param {Function} callback :: Pass in a callback that provides the settings.
 * @returns {Object} The current settings object.
 */
function saveSettings(callback) {
	fs.writeFile(pathToSettings, JSON.stringify(settings, null, 4), 'utf8', function(err) {
		if(err) {
			throw err;
		}

		if(typeof callback === 'function')
		{
			callback(settings)
		}
	});

	return settings;
}

function startWebServer() {
	server.listen(settings.port);

	app.get('*', function (req, res) {
		res.sendFile(path.join(__dirname, '/client', req.params[0]));
	});

	app.get('/', function(req, res) {
		res.sendFile(path.join(__dirname, '/client/index.html'));
	});

	// Declare all routes here. Return the object for the proper response.
	var routes = {
		'load_settings': function (data) {
			var res = {
				'settings': settings
			};

			return res;
		}
	};

	io.sockets.on('connection', function (client) {
		// Create an automatic response for a socket request for our "routes" object.
		// Eliminates need to declare socket.on and do socket.emit when we would always do so anyway.
		// The theory is that routes would always provide a response anyway (like AJAX).
		Object.keys(routes).forEach(function(key){
			client.on(key, function(incomingData) {
				var res = routes[key](incomingData);
				res.sysTime = getFormattedDate();
				client.emit(key, res);
			});
		});

		// Connection established, emit server time
		client.emit('connected', { sysTime: getFormattedDate() });
	});
}