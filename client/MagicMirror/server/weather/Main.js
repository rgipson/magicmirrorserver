Ext.define('MagicMirror.server.weather.Main', {
    extend: 'Ext.container.Container',
    requires: ['MagicMirror.server.weather.MainController'],
    controller: 'server.weather',
    alias: 'widget.server.weather'
});