Ext.define('MagicMirror.server.overall.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.server.overall',
    init: function() {
        socket.on('load_settings', Ext.Function.pass(function(data){
            this.getView().getForm().setValues({
                "google_account": data.settings.google.user_account.id
            });
        }, [], this));

        socket.emit('load_settings');
    },

    linkGoogleAccount: function() {

    },
});