Ext.define('MagicMirror.server.overall.Main', {
    extend: 'Ext.form.Panel',
    requires: ['MagicMirror.server.overall.MainController'],
    controller: 'server.overall',
    alias: 'widget.server.overall',
    flex: 1,
    border: false,
    defaults: {
        margin: '10 20'
    },
    items: [{
        xtype: 'fieldset',
        title: 'Google Settings',
        items: [{
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'textfield',
                disabled: true,
                emptyText: 'No account linked.',
                name: 'google_account',
                fieldLabel: 'Google Account'
            }, {
                xtype: 'button',
                iconCls: 'fa fa-google',
                text: 'Link Account',
                handler: 'linkGoogleAccount'
            }]
        }]
    }]
});