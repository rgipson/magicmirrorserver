Ext.define('MagicMirror.server.app.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.app.main',
    init: function() {
        this.getView().setActiveTab(this.lookup('overallTab'));
    }
});