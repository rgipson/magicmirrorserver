Ext.define('MagicMirror.server.app.Main', {
	extend: 'Ext.tab.Panel',
	requires: [
		'MagicMirror.server.overall.Main',
		'MagicMirror.server.weather.Main',
		'MagicMirror.server.app.MainController'
	],
	controller: 'app.main',
	title: 'MagicMirror Server Configuration',
	titleAlign: 'right',
	iconCls: 'fa fa-pencil-square-o fa-lg',
	width: '100%',
	height: '100%',
	tabPosition: 'left',
	border: false,
	header: {
		iconAlign: 'right'
	},
	tabBar: {
		tabRotation: 0,
		padding: '15px 20px 15px 10px'
	},
	defaults: {
		padding: 15,
		scrollable: true
	},
	items: [{
		title: 'Main Settings',
		iconCls: 'fa fa-cogs',
		reference: 'overallTab',
		xtype: 'server.overall'
	}, {
		title: 'Weather Widget',
		iconCls: 'fa fa-sun-o',
		reference: 'weatherTab',
		xtype: 'server.weather'
	}]
});